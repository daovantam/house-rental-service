FROM openjdk:8
ADD target/house-rental-service.jar house-rental-service.jar
EXPOSE 8083
RUN mkdir /images
ENTRYPOINT ["java", "-jar", "house-rental-service.jar"]