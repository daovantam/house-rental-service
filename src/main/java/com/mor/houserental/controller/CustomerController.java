package com.mor.houserental.controller;

import com.mor.houserental.dto.CustomerCreateDto;
import com.mor.houserental.dto.CustomerResponseDto;
import com.mor.houserental.factory.ResponseFactory;
import com.mor.houserental.internal.client.ManagementServiceClient;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {

  private final ManagementServiceClient managementServiceClient;
  private final ResponseFactory responseFactory;

  @Autowired
  public CustomerController(
      ManagementServiceClient managementServiceClient,
      ResponseFactory responseFactory) {
    this.managementServiceClient = managementServiceClient;
    this.responseFactory = responseFactory;
  }

  @PostMapping
  public ResponseEntity<Object> create(@RequestBody CustomerCreateDto customerCreateDto) {
    CustomerResponseDto created = Objects
        .requireNonNull(managementServiceClient.createCustomer(customerCreateDto).getBody())
        .getData();
    return responseFactory.created(created, CustomerResponseDto.class);
  }
}
