package com.mor.houserental.controller;

import com.mor.houserental.dto.response.CategoryResponseDto;
import com.mor.houserental.dto.response.DistrictResponseDto;
import com.mor.houserental.dto.response.HouseResponseDto;
import com.mor.houserental.dto.response.ProvinceResponseDto;
import com.mor.houserental.dto.response.SearchDtoResponse;
import com.mor.houserental.dto.response.WardResponseDto;
import com.mor.houserental.dto.search.SearchCriteriaDto;
import com.mor.houserental.factory.ResponseFactory;
import com.mor.houserental.service.SearchService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/search")
public class SearchController {

  private final ResponseFactory responseFactory;

  private final SearchService searchService;

  @Autowired
  public SearchController(ResponseFactory responseFactory,
      SearchService searchService) {
    this.responseFactory = responseFactory;
    this.searchService = searchService;
  }

  @PostMapping("/ward")
  ResponseEntity<Object> searchWard(@RequestBody SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<WardResponseDto> response = searchService.searchWard(searchCriteriaDto);
    return responseFactory.success(response, SearchDtoResponse.class);
  }

  @PostMapping("/district")
  ResponseEntity<Object> searchDistrict(@RequestBody SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<DistrictResponseDto> response = searchService.searchDistrict(searchCriteriaDto);
    return responseFactory.success(response, SearchDtoResponse.class);
  }

  @PostMapping("/province")
  ResponseEntity<Object> searchProvince(@RequestBody SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<ProvinceResponseDto> response = searchService.searchProvince(searchCriteriaDto);
    return responseFactory.success(response, SearchDtoResponse.class);
  }

  @PostMapping("/house")
  ResponseEntity<Object> searchHouse(@RequestBody SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<HouseResponseDto> response = searchService.searchHouse(searchCriteriaDto);
    return responseFactory.success(response, SearchDtoResponse.class);
  }

  @GetMapping("/categories")
  ResponseEntity<Object> getCategories() {
    List<CategoryResponseDto> response = searchService.getListCategory();
    return responseFactory.success(response, List.class);
  }

  @GetMapping("house/{id}")
  ResponseEntity<Object> getHouse(@PathVariable("id")UUID id) {
    HouseResponseDto response = searchService.getHouse(id);
    return responseFactory.success(response, HouseResponseDto.class);
  }
}
