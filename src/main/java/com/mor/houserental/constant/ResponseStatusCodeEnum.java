package com.mor.houserental.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatusCodeEnum {
  SUCCESS("RES200", "Success"),
  CREATED("RES201", "Created"),
  NO_PERMISSION("RES40000", "The client has no permission calling this API"),
  INVALID_PARAMETER("RES40001", "Invalid request parameter"),
  INTERNAL_SERVER_ERROR("ES50000", "Internal server error"),
  SCOPE_NOT_FOUND("RES40004", "Scope not found"),
  RESOURCE_NOT_FOUND("RES4040", "Resource not found"),
  FILE_TYPE_INCORRECT("RES4041", "File type incorrect"),
  CODE_INVALID("RES40002", "Code has already exists, please select another code");
  private final String code;
  private final String message;
}
