package com.mor.houserental.service.impl;

import com.mor.houserental.dto.response.CategoryResponseDto;
import com.mor.houserental.dto.response.DistrictResponseDto;
import com.mor.houserental.dto.response.HouseResponseDto;
import com.mor.houserental.dto.response.ProvinceResponseDto;
import com.mor.houserental.dto.response.SearchDtoResponse;
import com.mor.houserental.dto.response.WardResponseDto;
import com.mor.houserental.dto.search.SearchCriteriaDto;
import com.mor.houserental.factory.GeneralResponse;
import com.mor.houserental.internal.client.ManagementServiceClient;
import com.mor.houserental.service.SearchService;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService {

  private final ManagementServiceClient managementServiceClient;

  @Autowired
  public SearchServiceImpl(
      ManagementServiceClient managementServiceClient) {
      this.managementServiceClient = managementServiceClient;
  }

  @Override
  @Cacheable(value = "searchWard", key = "#searchCriteriaDto")
  public SearchDtoResponse<WardResponseDto> searchWard(SearchCriteriaDto searchCriteriaDto) {
    ResponseEntity<GeneralResponse<SearchDtoResponse<WardResponseDto>>> result = managementServiceClient
        .searchWard(searchCriteriaDto);
    return Objects.requireNonNull(result.getBody()).getData();
  }

  @Override
  @Cacheable(value = "searchDistrict", key = "#searchCriteriaDto")
  public SearchDtoResponse<DistrictResponseDto> searchDistrict(
      SearchCriteriaDto searchCriteriaDto) {
    ResponseEntity<GeneralResponse<SearchDtoResponse<DistrictResponseDto>>> result = managementServiceClient
        .searchDistrict(searchCriteriaDto);
    return Objects.requireNonNull(result.getBody()).getData();
  }

  @Override
  @Cacheable(value = "searchProvince", key = "#searchCriteriaDto")
  public SearchDtoResponse<ProvinceResponseDto> searchProvince(
      SearchCriteriaDto searchCriteriaDto) {
    ResponseEntity<GeneralResponse<SearchDtoResponse<ProvinceResponseDto>>> result = managementServiceClient
        .searchProvince(searchCriteriaDto);
    return Objects.requireNonNull(result.getBody()).getData();
  }

  @Override
  public SearchDtoResponse<HouseResponseDto> searchHouse(SearchCriteriaDto searchCriteriaDto) {
    ResponseEntity<GeneralResponse<SearchDtoResponse<HouseResponseDto>>> result = managementServiceClient
        .searchHouse(searchCriteriaDto);
    return Objects.requireNonNull(result.getBody()).getData();
  }

  @Override
  public HouseResponseDto getHouse(UUID id) {
    ResponseEntity<GeneralResponse<HouseResponseDto>> result = managementServiceClient.getHouse(id);
    return Objects.requireNonNull(result.getBody()).getData();
  }

  @Override
  public List<CategoryResponseDto> getListCategory() {
    ResponseEntity<GeneralResponse<List<CategoryResponseDto>>> responseEntity = managementServiceClient
        .getListCategory();
    return Objects.requireNonNull(responseEntity.getBody()).getData();
  }
}
