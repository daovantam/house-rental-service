package com.mor.houserental.service;

import com.mor.houserental.dto.response.CategoryResponseDto;
import java.util.List;

public interface BaseService {
  List<CategoryResponseDto> getListCategory();
}
