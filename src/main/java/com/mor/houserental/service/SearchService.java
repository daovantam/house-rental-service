package com.mor.houserental.service;

import com.mor.houserental.dto.response.DistrictResponseDto;
import com.mor.houserental.dto.response.HouseResponseDto;
import com.mor.houserental.dto.response.ProvinceResponseDto;
import com.mor.houserental.dto.response.SearchDtoResponse;
import com.mor.houserental.dto.response.WardResponseDto;
import com.mor.houserental.dto.search.SearchCriteriaDto;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public interface SearchService extends BaseService{

  SearchDtoResponse<WardResponseDto> searchWard(SearchCriteriaDto searchCriteriaDto);
  SearchDtoResponse<DistrictResponseDto> searchDistrict(SearchCriteriaDto searchCriteriaDto);
  SearchDtoResponse<ProvinceResponseDto> searchProvince(SearchCriteriaDto searchCriteriaDto);
  SearchDtoResponse<HouseResponseDto> searchHouse(SearchCriteriaDto searchCriteriaDto);
  HouseResponseDto getHouse(UUID id);
}
