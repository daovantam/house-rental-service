package com.mor.houserental.internal.client;

import com.mor.houserental.dto.CustomerCreateDto;
import com.mor.houserental.dto.CustomerResponseDto;
import com.mor.houserental.dto.response.CategoryResponseDto;
import com.mor.houserental.dto.response.DistrictResponseDto;
import com.mor.houserental.dto.response.HouseResponseDto;
import com.mor.houserental.dto.response.ProvinceResponseDto;
import com.mor.houserental.dto.response.SearchDtoResponse;
import com.mor.houserental.dto.response.WardResponseDto;
import com.mor.houserental.dto.search.SearchCriteriaDto;
import com.mor.houserental.factory.GeneralResponse;
import java.util.List;
import java.util.UUID;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "management-service/manage", fallbackFactory = ServiceClientFallBackFactory.class)
@RibbonClient(name = "management-service")
public interface ManagementServiceClient {

  @PostMapping(value = "api/v1/ward/search", produces = "application/json")
  ResponseEntity<GeneralResponse<SearchDtoResponse<WardResponseDto>>> searchWard(
      SearchCriteriaDto searchCriteriaDto);

  @PostMapping(value = "api/v1/district/search", produces = "application/json")
  ResponseEntity<GeneralResponse<SearchDtoResponse<DistrictResponseDto>>> searchDistrict(
      SearchCriteriaDto searchCriteriaDto);

  @PostMapping(value = "api/v1/province/search", produces = "application/json")
  ResponseEntity<GeneralResponse<SearchDtoResponse<ProvinceResponseDto>>> searchProvince(
      SearchCriteriaDto searchCriteriaDto);

  @PostMapping(value = "api/v1/house/search", produces = "application/json")
  ResponseEntity<GeneralResponse<SearchDtoResponse<HouseResponseDto>>> searchHouse(
      SearchCriteriaDto searchCriteriaDto);

  @GetMapping(value = "api/v1/house/{id}", produces = "application/json")
  ResponseEntity<GeneralResponse<HouseResponseDto>> getHouse(@PathVariable("id")
      UUID id);

  @PostMapping(value = "api/v1/customer", produces = "application/json")
  ResponseEntity<GeneralResponse<CustomerResponseDto>> createCustomer(CustomerCreateDto customerCreateDto);

  @GetMapping(value = "api/v1/category", produces = "application/json")
  ResponseEntity<GeneralResponse<List<CategoryResponseDto>>> getListCategory();
}
