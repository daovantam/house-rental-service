package com.mor.houserental.internal.client;


import com.mor.houserental.dto.CustomerCreateDto;
import com.mor.houserental.dto.CustomerResponseDto;
import com.mor.houserental.dto.response.CategoryResponseDto;
import com.mor.houserental.dto.response.DistrictResponseDto;
import com.mor.houserental.dto.response.HouseResponseDto;
import com.mor.houserental.dto.response.ProvinceResponseDto;
import com.mor.houserental.dto.response.SearchDtoResponse;
import com.mor.houserental.dto.response.WardResponseDto;
import com.mor.houserental.dto.search.SearchCriteriaDto;
import com.mor.houserental.factory.GeneralResponse;
import feign.hystrix.FallbackFactory;
import java.util.List;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
final class ServiceClientFallBackFactory implements FallbackFactory<ManagementServiceClient> {

  @Override
  public ManagementServiceClient create(Throwable throwable) {
    return new ManagementServiceClient() {
      @Override
      public ResponseEntity<GeneralResponse<SearchDtoResponse<WardResponseDto>>> searchWard(
          SearchCriteriaDto searchCriteriaDto) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<SearchDtoResponse<DistrictResponseDto>>> searchDistrict(
          SearchCriteriaDto searchCriteriaDto) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<SearchDtoResponse<ProvinceResponseDto>>> searchProvince(
          SearchCriteriaDto searchCriteriaDto) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<SearchDtoResponse<HouseResponseDto>>> searchHouse(
          SearchCriteriaDto searchCriteriaDto) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<HouseResponseDto>> getHouse(
          UUID id) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<CustomerResponseDto>> createCustomer(
          CustomerCreateDto customerCreateDto) {
        throw new RuntimeException(throwable);
      }

      @Override
      public ResponseEntity<GeneralResponse<List<CategoryResponseDto>>> getListCategory() {
        throw new RuntimeException(throwable);
      }
    };
  }
}
