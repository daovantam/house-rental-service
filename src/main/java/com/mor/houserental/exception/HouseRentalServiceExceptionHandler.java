package com.mor.houserental.exception;

import com.mor.houserental.constant.ResponseStatusCodeEnum;
import com.mor.houserental.factory.GeneralResponse;
import com.mor.houserental.factory.ResponseStatus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class HouseRentalServiceExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException ex) {
    log.error("ConstraintViolationException: ", ex);
    StringBuilder message = new StringBuilder();
    Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
    for (ConstraintViolation<?> violation : violations) {
      Path path = violation.getPropertyPath();
      String[] pathArr = StringUtils.splitByWholeSeparatorPreserveAllTokens(path.toString(), ".");
      message.append(pathArr[1]).append(violation.getMessage());
    }
    message = new StringBuilder(message.substring(0, message.length() - 1));
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER,
        HttpStatus.BAD_REQUEST,
        message.toString() + ",");
  }


  @ExceptionHandler(EntityNotFoundException.class)
  public final ResponseEntity<?> handleEntityNotFoundExceptions(EntityNotFoundException ex) {
    log.error("EntityNotFoundException: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.RESOURCE_NOT_FOUND,
        HttpStatus.NOT_FOUND,
        ex.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public final ResponseEntity<?> handleAllExceptions(Exception ex) {
    log.error("Exception: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
        ex.getMessage());
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    log.error("ConstraintViolationException: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER, HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    logger.error("HttpMediaTypeNotSupportedException: ", ex);
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getContentType());
    builder.append("Media type is not supported. Supported media types are ");
    ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER,
        HttpStatus.UNSUPPORTED_MEDIA_TYPE,
        builder.toString());
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    log.error("MethodArgumentNotValidException: ", ex);
    BindingResult results = ex.getBindingResult();
    List<ObjectError> errors = results.getAllErrors();
    List<String> errorsDetails = new ArrayList<>(10);
    for (ObjectError error : errors) {
      errorsDetails.add(error.getDefaultMessage());
    }
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER, HttpStatus.BAD_REQUEST, errorsDetails);
  }

  private ResponseEntity<Object> createErrorResponse(
      ResponseStatusCodeEnum response, HttpStatus status, List<String> errorsDetails) {
    ResponseStatus responseStatus = new ResponseStatus(response.getCode(), response.getMessage());
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setData(errorsDetails);
    responseObject.setTimestamp(new Date());
    responseObject.setStatus(responseStatus);
    responseObject.setTimestamp(new Date());
    return new ResponseEntity<>(responseObject, status);
  }

  @ExceptionHandler(RuntimeException.class)
  public final ResponseEntity<?> handleAllRunTimeExceptions(RuntimeException ex) {
    logger.error("RuntimeException: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
        ex.getMessage());
  }

  private ResponseEntity<Object> createErrorResponse(
      ResponseStatusCodeEnum response, HttpStatus status, String message) {
    ResponseStatus responseStatus = new ResponseStatus(response.getCode(), response.getMessage());
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setData(message);
    responseObject.setStatus(responseStatus);
    responseObject.setTimestamp(new Date());
    return new ResponseEntity<>(responseObject, status);
  }
}
