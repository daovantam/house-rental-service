package com.mor.houserental.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponseDto {

  private String name;
  private String phone;
  private String titleHouse;
  private String hostName;
}
