package com.mor.houserental.dto.search;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchCriteria implements Serializable {

  private String key;
  private String operation;
  private Object value;
  private boolean orPredicate;
}
