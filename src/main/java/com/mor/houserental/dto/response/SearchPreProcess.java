package com.mor.houserental.dto.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class SearchPreProcess<T> implements Serializable {

  private Pageable pageable;

  private Specification<T> specification;
}
