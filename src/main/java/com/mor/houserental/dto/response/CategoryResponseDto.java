package com.mor.houserental.dto.response;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class CategoryResponseDto implements Serializable {

  private UUID id;

  private String name;

  private String description;

  private String imagePath;

  private Timestamp updatedTimestamp;

  private String updateBy;
}
