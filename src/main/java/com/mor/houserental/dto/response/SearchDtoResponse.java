package com.mor.houserental.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SearchDtoResponse<T> implements Serializable {

  private Integer currentPage;
  private Integer totalPage;
  private Integer pageSize;
  private Long totalElement;
  private List<T> listObject;
}
