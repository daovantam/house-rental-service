package com.mor.houserental.dto.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@AllArgsConstructor
@Builder
public class WardResponseDto implements Serializable {

  private String id;

  private String name;

  private String type;

  private String districtName;

  private String provinceName;
}
